/**
 * 
 */
package util;

import java.util.Calendar;
import java.util.Date;

/**
 * @author Gabriel
 * Esta clase ofrece todos los servicios relacionados con la fecha
 *
 */
/**
 * @author Gabriel
 *
 */
public class DateUtil {
	
	/**
	 * Este m�todo devuelve el a�o de una fecha determinada
	 * @param pDate es la fecha en formato Date
	 * @return devuelve el anio
	 */
	public static int getAnio(Date pDate){
		Calendar cal =Calendar.getInstance();
		cal.setTime(pDate);			
		return cal.get(Calendar.YEAR);
	}
	

	/**
	 * Este metodo devuelve el mes
	 * @param pDate recibe una fecha de tipo date
	 * @return un entero que corresponde al mes
	 */
	public static int getMes(Date pDate){
		Calendar cal =Calendar.getInstance();
		cal.setTime(pDate);			
		return cal.get(Calendar.MONTH)+1;		
	}

	/**
	 * Este metodo reciber una fecha y si es sabado o domingo devuelve verdadero
	 * @param pFecha es la fecha a evaluar
	 * @return verdadero si es fin de semana, caso contrario devuelve falso.
	 */
	public static boolean isFinDeSemana(Date pFecha){
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);
		return  cal.get(Calendar.DAY_OF_WEEK)==Calendar.SUNDAY ||
				cal.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY;
	}
	/**
	 * Este metodo me devuelve el dia de la feecha ingresada por parametro
	 * @param pDate se envia la fecha completa
	 * @return devuelve el dia 
	 */
	public static int getDay(Date pDate){
		Calendar cal =Calendar.getInstance();
		cal.setTime(pDate);			
		return cal.get(Calendar.DAY_OF_MONTH);
	}
	
	
	/**
	 * Este metodo me devuelve el dia de la feecha ingresada por parametro
	 * @param pDia recibe el dia de la fecha
	 * @param pMes recibe el mes de la fecha
	 * @param pAnio recibe el anio de la fecha
	 * @return devuelve el dia del mes
	 */
	public static int getDay(int pDia, int pMes, int pAnio){
		Calendar cal =Calendar.getInstance();
		cal.set(pAnio,pMes-1, pDia);
		
		return cal.get(Calendar.DAY_OF_MONTH);
	}
	
	/**
	 * Este m�todo devuelve el ultimo dia del mes, se debe tener en cuenta que la variable cal va a ser modificada
	 * ya que la variable pasa por referencia.
	 * @param cal es la fecha a evaluar
	 * @return es el valor del ultimo dia del mes
	 */
	public static int getUltimoDiaDelMes(Calendar cal){
		int mes = getMes(cal.getTime())-1;
		int iFin = mes+1;
		while(mes<iFin){
			cal.add(Calendar.DAY_OF_MONTH, 1);
			mes = getMes(cal.getTime())-1;
		}
		cal.add(Calendar.DAY_OF_MONTH, -1);
				
		return getDay(cal.getTime());
	}
	
	/**
	 * Esta funcion sirve para mover una variable calendar hasta un dia especificado, se debe tener en cuenta que la variable cal va a ser modificada
	 * ya que la variable pasa por referencia.
	 * @param cal  es la variable a modificar
	 * @param pDayOfWeek corresponde a la constante del dia al que se quiere llegar
	 * @return devuelve una  variable Calentad con la nueva fecha modificada
	 */
	public static Calendar retrocederADia(Calendar cal, int pDayOfWeek){
		cal.add(Calendar.DAY_OF_MONTH, -1);
		while (cal.get(Calendar.DAY_OF_WEEK)!= pDayOfWeek)
			cal.add(Calendar.DAY_OF_MONTH, -1);
		
		return cal;				
	}

}
