package modulo1;

public class Ejercicio3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		2-	Realizar un programa identificado como Ejercicio3.java que permita escribir el siguiente texto
//		Tecla de escape			significado
//		\n				significa nueva l�nea
//		\t				significa un tab de espacio
//		\�	es para poner � (comillas dobles) dentro del texto por ejemplo �Belencita�
//		\\				se utiliza para escribir la \ dentro del texto, por ejemplo \algo\
//		\�				se utiliza para las �(comilla simple) para escribir por ejemplo �Princesita�
		
		System.out.println("\tTecla de escape\t\tSignificado");
		System.out.println("\n\t\\t\t\t\tSignifica un tab de espacio");
		System.out.println("\t\\n\t\t\tSignifica nueva liniea");
		System.out.println("\t\\\"\t\t\tEs para poner  \"(comillas dobles) dentro del texto por ejemplo \"Balencita\"");
		System.out.println("\t\\\\\t\t\tSe utiliza para poner la \\ dentro del texto, por ejmplo \\algo\\");
		System.out.println("\t\\\'\t\t\tSe utiliza para poner la\'(comilla simple) dentro del texto por ejemplo \'Princesita\'");

	}

}
