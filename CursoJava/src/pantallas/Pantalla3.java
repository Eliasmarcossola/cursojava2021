package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Pantalla3 {

	private JFrame frame;
	private JTextField textNota1;
	private JTextField textNota2;
	private JTextField textNota3;
	private JLabel lblNota2_1;
	private JLabel lblResult;
	private JLabel lblImagen;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla3 window = new Pantalla3();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla3() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 477, 362);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblPromedioDeNotas = new JLabel("promedio de notas ");
		lblPromedioDeNotas.setFont(new Font("Dialog", Font.PLAIN, 20));
		lblPromedioDeNotas.setBounds(141, 24, 170, 26);
		frame.getContentPane().add(lblPromedioDeNotas);
		
		JLabel lblNota = new JLabel("nota1");
		lblNota.setBounds(41, 101, 46, 14);
		frame.getContentPane().add(lblNota);
		//
		textNota1 = new JTextField();
		textNota1.setBounds(97, 98, 86, 20);
		frame.getContentPane().add(textNota1);
		textNota1.setColumns(10);
		
		JLabel lblNota2 = new JLabel("nota2");
		lblNota2.setBounds(41, 150, 46, 14);
		frame.getContentPane().add(lblNota2);
		
		textNota2 = new JTextField();
		textNota2.setBounds(97, 150, 86, 20);
		frame.getContentPane().add(textNota2);
		textNota2.setColumns(10);

		textNota3 = new JTextField();
		textNota3.setBounds(97, 200, 86, 20);
		frame.getContentPane().add(textNota3);
		textNota3.setColumns(10);
		
		lblNota2_1 = new JLabel("nota2");
		lblNota2_1.setBounds(41, 203, 46, 14);
		frame.getContentPane().add(lblNota2_1);
		
		lblResult = new JLabel("");
		lblResult.setBackground(Color.MAGENTA);
		lblResult.setOpaque(true);
		lblResult.setBounds(263, 156, 76, 29);
		frame.getContentPane().add(lblResult);
		
		JButton btnCalculaar = new JButton("Calculaar");
		btnCalculaar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				//obtengo los 3 valores de las  notas
				float fnota1 = Float.parseFloat(textNota1.getText());
				float fnota2 = Float.parseFloat(textNota2.getText());
				float fnota3 = Float.parseFloat(textNota3.getText());
				
				float fpromedio = (fnota1+fnota2+fnota3)/3;
				if(fpromedio>=7){
					
					lblResult.setText(Float.toString(fpromedio));
					
					lblResult.setBackground(Color.GREEN);
					lblImagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/iconos/dedoArriba_32px.png")));
					
				}
				else{
					lblResult.setText(Float.toString(fpromedio));
					lblResult.setBackground(Color.RED);
					lblImagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/iconos/dedoAbajo_32px.png")));
				}
			}
		});
		btnCalculaar.setBounds(207, 259, 89, 23);
		frame.getContentPane().add(btnCalculaar);
		
		JButton btnLImpiar = new JButton("Limpiar");
		btnLImpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textNota1.setText("");
				textNota2.setText("");
				textNota3.setText("");
				
				lblResult.setText("");
				lblResult.setBackground(Color.MAGENTA);
				lblImagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/iconos/nieve_32px.png")));		
				
			}
		});
		btnLImpiar.setBounds(334, 259, 89, 23);
		frame.getContentPane().add(btnLImpiar);
		
		lblImagen = new JLabel("");
		lblImagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/iconos/nieve_32px.png")));
		lblImagen.setBounds(334, 67, 46, 48);
		frame.getContentPane().add(lblImagen);

		
	}
}
