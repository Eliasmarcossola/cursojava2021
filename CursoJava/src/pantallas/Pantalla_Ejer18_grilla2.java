package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.DefaultComboBoxModel;

public class Pantalla_Ejer18_grilla2 {

	private JFrame frame;
	private JTable table;
	//esta son todas las tablas resueltas que estan Object
	private String tablas[][];
	private String titulos[]; 
	private JComboBox cmbDesde;
	private JComboBox cmbHasta;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla_Ejer18_grilla2 window = new Pantalla_Ejer18_grilla2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla_Ejer18_grilla2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 873, 457);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(21, 67, 716, 236);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();


		scrollPane.setViewportView(table);
		
		//-----------------aca se completan los combos con los valores 
		// para lo cual utilizo un solo array para los dos
		String strTablas[]= new String[10];
		for(int i=0;i<10;i++)
			strTablas[i] = Integer.toString(i+1);
		
		cmbDesde = new JComboBox();
		cmbDesde.setModel(new DefaultComboBoxModel(strTablas));		
		cmbDesde.setBounds(747, 122, 100, 20);
		
		frame.getContentPane().add(cmbDesde);

		cmbHasta = new JComboBox();
		cmbHasta.setBounds(747, 207, 100, 20);
		cmbHasta.setModel(new DefaultComboBoxModel(strTablas));
		
		frame.getContentPane().add(cmbHasta);
		//*************** fin carga de combos *************************
		JLabel lblDesde = new JLabel("Desde");
		lblDesde.setBounds(773, 97, 46, 14);
		frame.getContentPane().add(lblDesde);
		
		JLabel lblHasta = new JLabel("Hasta");
		lblHasta.setBounds(773, 182, 46, 14);
		frame.getContentPane().add(lblHasta);
		
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//---como se baja desde object a un valor int
				Object objDesde = cmbDesde.getSelectedItem();
				String strDesde = (String)objDesde;
				int iDesde = Integer.parseInt(strDesde);
				//---- fin de cocnversion-------------
				
				//------ conversion en una linea
				int iHasta=Integer.parseInt((String)cmbHasta.getSelectedItem());
				//----------- fin de convesion
				int iCantCol = iHasta-iDesde+1;
				titulos = new String[iCantCol];
				tablas = new String[10][iCantCol];
				
				for(int col=0;col<iCantCol;col++){
					titulos[col]="tablas del " + (col+iDesde);
					for(int fila=0;fila<10;fila++)
						tablas[fila][col] = (col +iDesde)+ " x " + fila + " = " + (fila*(col+iDesde));
				}
				table.setModel(new DefaultTableModel(
						tablas,
						titulos
					));
			}
		});
		btnCalcular.setBounds(747, 288, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		JLabel lblNewLabel = new JLabel("Tablas de  multiplicar");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 32));
		lblNewLabel.setBounds(187, 11, 344, 39);
		frame.getContentPane().add(lblNewLabel);
	}
}
