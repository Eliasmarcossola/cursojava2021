package pantallas.objetos.juegoStrategy;

import pantallas.objetos.juegoFactory.JuegoPiedraPapelTijera;

public class TijeraGanaStrategy extends ComparacionStrategy {

	@Override
	public int getResultado() {
		return JuegoPiedraPapelTijera.GANA;
	}

	@Override
	public String getTextoResultado() {
		StringBuffer strTexto = new StringBuffer(ComparacionStrategy.juegoPiedraPapelTijera.getJugador().getAlias());
		strTexto.append(" con TIJERA GANA contra ");
		strTexto.append(ComparacionStrategy.juegoPiedraPapelTijera.getContrincante().getJugador().getAlias());
		strTexto.append(" que eligi� PAPEL");
		
		return strTexto.toString();	
	}

	@Override
	public boolean isMe(JuegoPiedraPapelTijera pJuegoPpt) {
		return 	ComparacionStrategy.juegoPiedraPapelTijera.getValor() 					== juegoPiedraPapelTijera.TIJERA 	&&
				ComparacionStrategy.juegoPiedraPapelTijera.getContrincante().getValor() == JuegoPiedraPapelTijera.PAPEL;
	}

}
