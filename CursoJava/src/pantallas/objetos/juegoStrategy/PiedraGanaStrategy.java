package pantallas.objetos.juegoStrategy;


import pantallas.objetos.juegoFactory.JuegoPiedraPapelTijera;

public class PiedraGanaStrategy extends ComparacionStrategy{

	@Override
	public boolean isMe(JuegoPiedraPapelTijera pJuegoPpt) {
		
		return 	pJuegoPpt.getValor()					== JuegoPiedraPapelTijera.PIEDRA &&
				pJuegoPpt.getContrincante().getValor() 	== JuegoPiedraPapelTijera.TIJERA;
	}

	@Override
	public int getResultado() {		
		return 1;
	}

	@Override
	public String getTextoResultado() {
		StringBuffer strTexto = new StringBuffer(ComparacionStrategy.juegoPiedraPapelTijera.getJugador().getAlias());
		strTexto.append(" con PIEDRA le gan� a ");
		strTexto.append(ComparacionStrategy.juegoPiedraPapelTijera.getContrincante().getJugador().getAlias());
		strTexto.append(" con TIJERA");
		return strTexto.toString();
	}


}
