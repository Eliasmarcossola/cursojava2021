package pantallas.objetos.juegoStrategy;

import pantallas.objetos.juegoFactory.JuegoPiedraPapelTijera;

public class PapelGanaStrategy extends ComparacionStrategy {

	@Override
	public int getResultado() {
		return juegoPiedraPapelTijera.GANA;
	}

	@Override
	public String getTextoResultado() {
		StringBuffer strTexto = new StringBuffer(ComparacionStrategy.juegoPiedraPapelTijera.getJugador().getAlias());
		strTexto.append(" con PAPEL GANO a ");
		strTexto.append(ComparacionStrategy.juegoPiedraPapelTijera.getContrincante().getJugador().getAlias());
		strTexto.append(" que eligi� PIEDRA");
		
		return strTexto.toString();
	}

	@Override
	public boolean isMe(JuegoPiedraPapelTijera pJuegoPpt) {		
		return 	ComparacionStrategy.juegoPiedraPapelTijera.getValor() 					== juegoPiedraPapelTijera.PAPEL 	&&
				ComparacionStrategy.juegoPiedraPapelTijera.getContrincante().getValor() == JuegoPiedraPapelTijera.PIEDRA	;
	}

}
