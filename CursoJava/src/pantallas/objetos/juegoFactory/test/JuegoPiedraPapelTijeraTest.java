package pantallas.objetos.juegoFactory.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import pantallas.objetos.Jugador;
import pantallas.objetos.juegoFactory.JuegoPiedraPapelTijera;
import pantallas.objetos.juegoFactory.Papel;
import pantallas.objetos.juegoFactory.Piedra;
import pantallas.objetos.juegoFactory.Tijera;
import pantallas.objetos.juegoStrategy.ComparacionStrategy;

public class JuegoPiedraPapelTijeraTest {
	JuegoPiedraPapelTijera piedra;
	JuegoPiedraPapelTijera papel ;
	JuegoPiedraPapelTijera tijera;
	
	Jugador jug1 = new Jugador("Juan", "Perez", "juanchi");
	Jugador jug2 = new Jugador("Recardo", "Arjona", "richi");
	
	@Before
	public void setUp() throws Exception {
	
		piedra = new Piedra();
		
		papel = new Papel();
		tijera = new Tijera();
		
	}

	@After
	public void tearDown() throws Exception {
		piedra = null;
		papel = null;
		tijera = null;
	}

	@Test
	public void testGetInstancePIEDRA() {
		JuegoPiedraPapelTijera jppT=JuegoPiedraPapelTijera.getInstance(JuegoPiedraPapelTijera.PIEDRA);
		assertTrue(jppT instanceof Piedra);
	}

	@Test
	public void testGetInstancePAPEL() {
		JuegoPiedraPapelTijera jppT=JuegoPiedraPapelTijera.getInstance(JuegoPiedraPapelTijera.PAPEL);
		assertTrue(jppT instanceof Papel);
	}
	@Test
	public void testGetInstanceTIJERA() {
		JuegoPiedraPapelTijera jppT=JuegoPiedraPapelTijera.getInstance(JuegoPiedraPapelTijera.TIJERA);
		assertTrue(jppT instanceof Tijera);
	}

	@Test
	public void testPIEDRApierdeAPAPELcomparar() {
		
		assertEquals(JuegoPiedraPapelTijera.PIERDE, piedra.comparar(papel));
	}

	@Test
	public void testPIEDRAganaTIJERAcomparar() {
		assertEquals(JuegoPiedraPapelTijera.GANA, piedra.comparar(tijera));
	}

	@Test
	public void testGetInstancePIEDRAganaTIJERAcomparar() {
		String strTexto = "juanchi con PIEDRA le gan� a richi con TIJERA";
		
		piedra.setJugador(jug1);
		tijera.setJugador(jug2);
		
		piedra.comparar(tijera);
		assertEquals(strTexto, piedra.getResultado());
	}

}
